package com.marfeel.test.webCrawler.it;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import com.marfeel.test.webCrawler.config.MongoConfiguration;
import com.marfeel.test.webCrawler.config.MvcConfiguration;
import com.marfeel.test.webCrawler.mongodb.VisitedSite;
import com.marfeel.test.webCrawler.mongodb.repository.VisitedSiteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {MvcConfiguration.class, MongoConfiguration.class})
@WebAppConfiguration
public class CrawlerIT{
	
	private static String port;
	private HttpHeaders headers;
	private static final String HTTP_ADDRESS = "houseonthehill";

	@Autowired
	VisitedSiteRepository visitedSiteRepository;
	
	@Before
	public void setup() {
		port = System.getProperty("servlet.port", "8080");
		headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	}

	@Test
	public void crawlerPostConnection() throws IOException {
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>("[]", headers);
	    
		ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port +"/crawl", request ,String.class);
		
		assertEquals(response.getStatusCode(), HttpStatus.OK);
	}
	
	@Test
	public void crawlerPostGoogle() throws IOException, InterruptedException {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<String> request = new HttpEntity<String>("[{\"url\":\""+HTTP_ADDRESS+"\"}]", headers); 
	    
		ResponseEntity<String> response = restTemplate.postForEntity("http://localhost:" + port +"/crawl", request ,String.class);
		
		Thread.sleep(5000);
		
		assertEquals(response.getStatusCode(), HttpStatus.OK);
		
		List<VisitedSite> visitedSiteList = visitedSiteRepository.findByUrl(HTTP_ADDRESS);
		
		assertNotNull(visitedSiteList);
		assertFalse(visitedSiteList.isEmpty());
		assertFalse(visitedSiteList.get(0).isMarfeelizable());
		visitedSiteRepository.deleteAll(visitedSiteList);
	}
	
	
}
