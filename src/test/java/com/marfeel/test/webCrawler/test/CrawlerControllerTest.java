package com.marfeel.test.webCrawler.test;


import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.marfeel.test.webCrawler.controller.CrawlerController;
import com.marfeel.test.webCrawler.service.CrawlerService;

@RunWith(MockitoJUnitRunner.class)
public class CrawlerControllerTest {
	
	@Mock
	CrawlerService crawlerService;
	
	@InjectMocks
	CrawlerController crawlerController;
	
	@Test
	public void should_call_service() {
		crawlerController.crawl(Collections.emptyList());
		Mockito.verify(crawlerService, Mockito.times(1)).crawlSites(Collections.emptyList());
	}
	
}