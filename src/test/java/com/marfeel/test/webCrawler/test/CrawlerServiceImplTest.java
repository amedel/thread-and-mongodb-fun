package com.marfeel.test.webCrawler.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.marfeel.test.webCrawler.launcher.CrawlLauncher;
import com.marfeel.test.webCrawler.request.body.Site;
import com.marfeel.test.webCrawler.service.DefaultCrawlerService;

@RunWith(MockitoJUnitRunner.class)
public class CrawlerServiceImplTest {
	
	@Mock
	CrawlLauncher crawlLauncher;
	
	@InjectMocks
	DefaultCrawlerService crawlerService;
	
	@Test
	public void should_crawl_two_times() throws Exception {

		List<Site> sites =  new ArrayList<Site>(4);
		Site site= new Site();
		site.setUrl("");
		sites.add(site);
		
		site= new Site();	
		site.setUrl("www.google.com");
		sites.add(site);

		site= new Site();
		site.setUrl("www.yahoo.com");
		sites.add(site);
		
		crawlerService.crawlSites(sites);
		
		Mockito.verify(crawlLauncher,Mockito.times(2)).launchThread(Mockito.any(Site.class));
		
	}
	
}
