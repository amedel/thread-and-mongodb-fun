package com.marfeel.test.webCrawler.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marfeel.test.webCrawler.launcher.CrawlLauncher;
import com.marfeel.test.webCrawler.request.body.Site;

@Service
public class DefaultCrawlerService implements CrawlerService {

	@Autowired
	private CrawlLauncher crawlLauncher;
	
	@Override
	public void crawlSites(List<Site> sites) {
		sites.stream()
		     .filter(site -> StringUtils.isNotBlank(site.getUrl()))
		     .forEach(crawlLauncher::launchThread);
	}

}
