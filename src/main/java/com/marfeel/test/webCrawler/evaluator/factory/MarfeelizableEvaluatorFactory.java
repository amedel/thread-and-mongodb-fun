package com.marfeel.test.webCrawler.evaluator.factory;

import com.marfeel.test.webCrawler.evaluator.MarfeelizableEvaluator;

public interface MarfeelizableEvaluatorFactory {

	MarfeelizableEvaluator create();
	
}
