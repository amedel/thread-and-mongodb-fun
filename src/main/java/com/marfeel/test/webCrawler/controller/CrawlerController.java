package com.marfeel.test.webCrawler.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.marfeel.test.webCrawler.request.body.Site;
import com.marfeel.test.webCrawler.service.CrawlerService;

@RestController
public class CrawlerController {

	@Autowired
	private CrawlerService crawlerService;
	
	@PostMapping(value="/crawl", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void crawl(@RequestBody(required=true) List<Site> sites) {
		crawlerService.crawlSites(sites);
	}
	
}
